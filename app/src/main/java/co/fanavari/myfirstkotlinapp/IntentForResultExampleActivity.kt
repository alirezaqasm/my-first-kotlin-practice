package co.fanavari.myfirstkotlinapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import co.fanavari.myfirstkotlinapp.databinding.ActivityForResultExampleBinding

class IntentForResultExampleActivity : AppCompatActivity() {

    private lateinit var binding: ActivityForResultExampleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityForResultExampleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonSendResult.setOnClickListener {
            val intent = Intent()
            var message = binding.editTextResult.text.toString()
            intent.putExtra("id",message)
            setResult(Activity.RESULT_OK,intent)
            finish()

        }
    }
}