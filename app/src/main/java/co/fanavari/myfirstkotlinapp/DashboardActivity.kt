package co.fanavari.myfirstkotlinapp

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import co.fanavari.myfirstkotlinapp.databinding.ActivityDashboardBinding


class DashboardActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDashboardBinding
    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_dashboard)


        binding= ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //val backB: AppCompatImageButton = findViewById(R.id.backB)
        //val logoutB: AppCompatImageButton = findViewById(R.id.logOutB)

        //val todoB: MaterialButton = findViewById(R.id.todoB)

        /*val layoutCard: MaterialCardView = findViewById(R.id.layoutCards)

        backB.setOnClickListener {
            Toast.makeText(this, "back Button clicked!", Toast.LENGTH_SHORT).show()
        }*/

        binding.layoutCards.setOnClickListener {
            Toast.makeText(this,R.string.test_app,Toast.LENGTH_LONG).show()
        }
        binding.lifeCycleCard.setOnClickListener {
           val message : String = binding.textViewClassMentor.text.toString()
           val intent = Intent( this,IntentExampleActivity::class.java)
            intent.putExtra(Constant.MENTOR_NAME,message)

            startActivity(intent)

        }

        //binding.internetCard.setOnClickListener {

        //    val intent = Intent(this,IntentExampleActivity::class.java)

       // }


    }
    fun openMainActivity(view: View){
        val intent = Intent(this,MainActivity::class.java)


        startActivity(intent)
    }

}