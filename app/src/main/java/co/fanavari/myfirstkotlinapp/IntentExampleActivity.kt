package co.fanavari.myfirstkotlinapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import co.fanavari.myfirstkotlinapp.databinding.ActivityIntentExampleBinding
import kotlin.String as KotlinString

class IntentExampleActivity : AppCompatActivity() {
    private lateinit var binding: ActivityIntentExampleBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIntentExampleBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val openIntentForResultExampleActivity =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (it.resultCode == Activity.RESULT_OK) {
                    showToast(it.data?.getStringExtra("id").toString())
                }
            }
        binding.buttonOpenForResult.setOnClickListener {
            openIntentForResultExampleActivity.launch(
                Intent(this, IntentForResultExampleActivity::class.java).apply {
                    putExtra("ID", "1")

                }
            )
            binding.buttonUrl.setOnClickListener {
                openUrl()

            }
            binding.buttonSendEmail.setOnClickListener {
                composeEmail(arrayOf("qsmzadhl17@gmail.com"), "mailto my teacher")
            }
            binding.buttonSetAlarm.setOnClickListener {
                createAlarm("Alarm",4,30)
            }

        }
    }

    fun openUrl() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://fanavari.co/android-corse/"))
        startActivity(intent)
    }

    fun composeEmail(addresses: kotlin.Array<kotlin.String>, subject: kotlin.String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_EMAIL, addresses)
            putExtra(Intent.EXTRA_SUBJECT, subject)
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }


    fun createAlarm(message: kotlin.String, hour: Int, minutes: Int) {
        val intent = Intent(AlarmClock.ACTION_SET_ALARM).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
            putExtra(AlarmClock.EXTRA_HOUR, hour)
            putExtra(AlarmClock.EXTRA_MINUTES, minutes)
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }


}

    private fun Context.showToast(message: KotlinString, duration: Int = Toast.LENGTH_SHORT){
        Toast.makeText(this, message, duration).show()
    }


